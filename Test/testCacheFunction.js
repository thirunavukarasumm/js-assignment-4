let cacheFunction = require("../cacheFunction.js")

function cb(n) {
    if (typeof n === "object") {
        let square = [];
        for (i in n) {
            square.push(n[i] * n[i]);
        }
        return square;
    } else {
        return [n * n];
    }
}

let argumentTracker = cacheFunction(cb);
console.log(argumentTracker(1, 2, 3));
console.log(argumentTracker(1, 2, 3));
console.log(argumentTracker(2));
console.log(argumentTracker(2));
console.log(argumentTracker(1, 2, 3));

