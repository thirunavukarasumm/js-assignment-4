

let limitFunctionCallCount = require("../limitFunctionCallCount");

function cb() {
    return "CallBack function";
}


let limitCallCount = limitFunctionCallCount(cb, 3);

console.log(limitCallCount());
console.log(limitCallCount());
console.log(limitCallCount());
console.log(limitCallCount()); // Returns null as the count exceeded 3.
