

let counterFactoryObject = require("../counterFactory.js")(); // require returns a function and is executed here.


counterFactoryObject.increment();
counterFactoryObject.increment();
counterFactoryObject.increment();
counterFactoryObject.increment();
counterFactoryObject.decrement();
counterFactoryObject.decrement();


console.log(counterFactoryObject.counter); // To look at the current value in the counter variable.
console.log(counterFactoryObject.increment()); // To look at the incremented counter value.
console.log(counterFactoryObject.decrement()); // To look at the decremented counter value.


