// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.

function counterFactory() {
    function CounterConstructor() {
        this.counter = 0;
        this.increment = function () {
            this.counter += 1;
            return this.counter;
        };
        this.decrement = function () {
            this.counter -= 1;
            return this.counter;
        };
    }

    return new CounterConstructor();

}

module.exports = counterFactory;