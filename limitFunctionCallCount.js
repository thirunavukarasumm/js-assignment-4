// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {

    function countTracker() {
        if (n === 0) {
            return "Function call count exceeded."; // Returns this if the function call exceeds the given count.
        } else {
            n -= 1;
            let resultOfCB = cb();
            if (resultOfCB === undefined) {
                return null; // Returns null if cb() doesn't return a value.
            }
            else {
                return resultOfCB;
            }
        }

    }
    return countTracker;
}



module.exports = limitFunctionCallCount;


